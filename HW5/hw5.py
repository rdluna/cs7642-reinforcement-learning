from sys import argv


class PatronFunction():
    def __init__(self, numOfPatrons, instigator, peacemaker):
        self.n = numOfPatrons
        self.instigatorPosition = instigator
        self.peacemakerPosition = peacemaker

    def evaluate(self, x):
        if len(x) != self.n:
            raise AttributeError("Episode has wrong size")
        instigatorIn = x[self.instigatorPosition]
        peacemakerIn = x[self.peacemakerPosition]
        if peacemakerIn:
            return 0
        elif instigatorIn:
            return 1
        else:
            return 0


def computeNewH(h, x, y):
    if y == 0:
        Y = 0
    elif y == 1:
        Y = 1
    else:
        raise AttributeError("Can not transform y")

    res = set()
    for f in h:
        if Y == f.evaluate(x):
            res.add(f)

    return res


if __name__ == '__main__':
    if len(argv) == 1:
        raise AttributeError("No arguments passed")

    inputFile = argv[1]

    with open(inputFile, 'r') as file:
        numOfPatrons = int(file.readline())
        atEstablishment = eval(file.readline().replace('{', '[').replace('}', ']'))
        fightOccurred = eval(file.readline().replace('{', '[').replace('}', ']'))

    H = set()

    for i in range(numOfPatrons):
        for p in range(numOfPatrons):
            if i != p:
                fct = PatronFunction(numOfPatrons, i, p)
                H.add(fct)

    res = []
    Hprime = H.copy()
    currentEval = set()

    for idx, episode in enumerate(atEstablishment):
        for f in Hprime:
            currentEval.add(f.evaluate(episode))
        if currentEval.__len__() == 0:
            raise RuntimeError("Invalid function")
        if currentEval.__len__() == 1:
            res.extend(currentEval)
        if currentEval.__len__() == 2:
            res.append(-1)
            Hprime = computeNewH(Hprime, episode, fightOccurred[idx])
        currentEval.clear()

    print(res.__str__()[1:-1])
