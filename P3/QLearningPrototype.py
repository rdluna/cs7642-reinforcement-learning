import pandas as pd
import numpy as np


class QLearnerPrototype:
    def __init__(self, env, alphaDecay):
        self.episodes = 1000000
        self.gamma = 0.90
        self.alpha = self.alphaInit = 0.90
        self.alphaDecay = alphaDecay
        self.nS = 8
        self.nA = 5
        self.Q1 = self.Q2 = np.zeros([self.nS, self.nS, 2, self.nA, self.nA])
        self.env = env
        self.difference = []
        self.episode = []

    def update_alpha(self, episode):
        if self.alphaDecay == 'lin':
            self.alpha = self.alphaInit - 9e-7*episode
        if self.alphaDecay == 'log':
            self.alpha = self.alphaInit - 0.065*np.log(episode)
        if self.alphaDecay == 'exp':
            self.alpha = self.alphaInit * np.e ** (-7e-6 * episode)
        if self.alpha > self.alphaInit:
            self.alpha = self.alphaInit

    def save(self, name):
        output = pd.DataFrame()
        output['Episode'] = self.episode
        output['Difference'] = self.difference
        output.to_csv('.\\Output\\' + name, index=False)
