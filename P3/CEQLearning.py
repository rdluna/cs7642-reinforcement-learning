import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cvxopt as cvx


class Player:
    def __init__(self, id, hasBall=None):
        self.id = id
        self.score = 0
        self.position = 0
        self.hasBall = hasBall


class Soccer:
    def __init__(self, player1, player2):
        np.random.seed(112357)
        self.player1 = player1
        self.player2 = player2
        self.goal1 = [0, 4]
        self.goal2 = [3, 7]
        self.possesion = np.random.randint(2)
        self.ballPosition = player1.position

    def state(self):
        return [self.player1.position, self.player2.position, int(self.possesion)]

    def new(self):
        startPosition = np.random.choice([1, 2, 5, 6], 2, replace=False)
        self.player1.position = startPosition[0]
        self.player2.position = startPosition[1]
        if np.random.randint(2) == 0:
            self.possesion = self.player1.hasBall
            self.ballPosition = self.player1.position
        else:
            self.possesion = self.player2.hasBall
            self.ballPosition = self.player2.position

    def move(self, player, action):
        if action == 0 and player.position > 3:
            offset = -4
        elif action == 1 and player.position not in self.goal2:
            offset = 1
        elif action == 2 and player.position < 4:
            offset = 4
        elif action == 3 and player.position not in self.goal1:
            offset = -1
        else:
            offset = 0
        return player.position + offset

    def action(self, player1, player2, action1, action2):
        move1 = self.move(player1, action1)
        move2 = self.move(player2, action2)

        if move1 != player2.position:
            player1.position = move1
        else:
            self.possesion = player2.hasBall

        if move2 != player1.position:
            player2.position = move2
        else:
            self.possesion = player1.hasBall

        if self.possesion:
            self.ballPosition = player1.position
        else:
            self.ballPosition = player2.position

    def step(self, action1, action2):
        if np.random.randint(2) == 0:
            self.action(self.player1, self.player2, action1, action2)
        else:
            self.action(self.player2, self.player1, action2, action1)

        if self.ballPosition in self.goal1:
            score1 = 100
            score2 = -score1
            done = True
        elif self.ballPosition in self.goal2:
            score1 = -100
            score2 = -score1
            done = True
        else:
            score1 = score2 = 0
            done = False

        return self.state(), score1, score2, done


def plotResults(episodes, difference, filename):
    plt.figure(figsize=(10, 10))
    plt.plot(episodes, difference)
    plt.ylim([0, 0.5])
    plt.xlim([0, 1e06])
    plt.xlabel('Episode')
    plt.ylabel('Difference in state')
    plt.savefig('.\\Output\\' + filename)
    plt.close()


def saveResults(episodes, difference, filename):
    output = pd.DataFrame()
    output['Episode'] = episodes
    output['Difference'] = difference
    output.to_csv('.\\Output\\' + filename, index=False)


def solve_ceq(Q1, Q2):
    A = np.zeros((3 * 5 ** 2 - 2 * 5 + 2, 5 ** 2 + 1))
    A[0:2 * 5 ** 2 - 2 * 5, 0] = 1
    A[2 * 5 ** 2 - 2 * 5:-2, 1:] = np.eye(5**2) * -1
    A[-2, 1:] = 1
    A[-1, 1:] = -1

    for episode in range(5):
        Q1_ = Q1[episode] - Q1
        Q1_ = np.delete(Q1_, (episode), axis=0)
        A[(5 - 1)*episode:(5 - 1)*(episode + 1), 5*episode + 1:5*(episode + 1) + 1] = Q1_
        Q2_ = (np.repeat(Q2[:, episode].reshape((5, 1)), 5 - 1, axis=1) - np.delete(Q2, (episode), axis=1)).T
        if episode == 5-1:
            Q2_ = Q2_[:, :-1]
        A[(5 - 1) * episode + 5 * (5 - 1):(5 - 1)*episode + 5**2 -1, episode+1:5**2:5] = Q2_

    position2 = np.zeros((3 * 5 ** 2 - 2 * 5 + 2, 1))
    position2[-2:, 0] = np.array([1, -1])
    c = np.zeros((5**2 + 1, 1))
    c[0] = -1

    sol = cvx.solvers.lp(cvx.matrix(c), cvx.matrix(A), cvx.matrix(position2),
                         solver='glpk', options={'glpk':{'msg_lev':'GLP_MSG_OFF'}})

    if sol['x'] is None:
        return 0, 0
    else:
        r1 = np.matmul(Q1.flatten(), sol['x'][1:])[0]
        r2 = np.matmul(Q2.transpose().flatten(), sol['x'][1:])[0]
        return r1, r2


def ceqLearning(env, player1, player2):
    maxEpisodes = 1000000
    alpha = 0.9
    alphaInit = 0.9
    gamma = 0.9

    Q1 = np.zeros([8, 8, 2, 5, 5])
    Q2 = np.zeros([8, 8, 2, 5, 5])

    env.new()
    done = False

    difference = []
    episodes = []

    for episode in range(maxEpisodes):
        if done:
            env.new()
        position1 = player1.position
        position2 = player2.position
        possesion = env.possesion

        Qp = Q1[2, 1, 1, 2, 4]

        action1 = np.random.choice(5)
        action2 = np.random.choice(5)

        Qa_state = Q1[player1.position, player2.position, env.possesion]
        Qb_state = Q2[player1.position, player2.position, env.possesion]

        _, score1, score2, done = env.step(action1, action2)

        r1, r2 = solve_ceq(Qa_state, Qb_state)

        Q1[position1, position2, possesion, action1, action2] =\
            (1 - alpha) * Q1[position1, position2, possesion, action1, action2] +\
            alpha * ((1 - gamma) * score1 + gamma * r1)

        Q2[position1, position2, possesion, action1, action2] =\
            (1 - alpha) * Q2[position1, position2, possesion, action1, action2] +\
            alpha * ((1 - gamma) * score2 + gamma * r2)

        if [position1, position2, possesion, action1, action2] == [2, 1, 1, 2, 4]:
            difference.append(abs(Q1[2, 1, 1, 2, 4] - Qp))
            episodes.append(episode)
            print("\rIteration %i alpha: %0.4f, difference: %0.4f " % (episode, alpha, difference[-1]), end='')

        alpha = alphaInit * np.e ** (-7e-6 * episode)

    plotResults(episodes, difference, "CEQLearner_exp.png")
    saveResults(episodes, difference, "CEQLearner_exp.csv")


player1 = Player(id=1, hasBall=0)
player2 = Player(id=2, hasBall=1)
env = Soccer(player1, player2)
ceqLearning(env, player1, player2)

