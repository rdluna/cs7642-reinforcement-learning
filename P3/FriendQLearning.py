from P3.QLearningPrototype import QLearnerPrototype
import numpy as np


class Player:
    def __init__(self, id, hasBall):
        self.id = id
        self.score = 0
        self.position = 0
        self.hasBall = hasBall


class Soccer:
    def __init__(self):
        np.random.seed(112357)
        self.player1 = Player(1, False)
        self.player2 = Player(2, True)
        self.fieldSize = (2, 4)
        self.goal1 = [0, 4]
        self.goal2 = [3, 7]
        self.possesion = np.random.choice([0, 1])
        self.ballPosition = self.player1.position

    def state(self):
        return [self.player1.position, self.player2.position, int(self.possesion)]

    def new(self):
        startPosition = np.random.choice([1, 2, 5, 6], 2, replace=False)
        self.player1.position = startPosition[0]
        self.player2.position = startPosition[1]
        if np.random.choice([0, 1]):
            self.possesion = self.player1.hasBall
            self.ballPosition = self.player1.position
        else:
            self.possesion = self.player2.hasBall
            self.ballPosition = self.player2.position

    def move(self, player, action):
        if action == 0 and player.position > 3:
            offset = -4
        elif action == 1 and player.position not in self.goal2:
            offset = 1
        elif action == 2 and player.position < 4:
            offset = 4
        elif action == 3 and player.position not in self.goal1:
            offset = -1
        else:
            offset = 0
        return player.position + offset

    def action(self, player1, player2, action1, action2):
        move1 = self.move(player1, action1)
        move2 = self.move(player2, action2)
        if move1 != player2.position:
            player1.position = move1
        else:
            self.possesion = player2.hasBall
        if move2 != player1.position:
            player2.position = move2
        else:
            self.possesion = player1.hasBall
        if self.possesion:
            self.ballPosition = player1.position
        else:
            self.ballPosition = player2.position

    def step(self, action1, action2):
        if np.random.sample([0, 1]):
            self.action(self.player1, self.player2, action1, action2)
        else:
            self.action(self.player2, self.player1, action2, action1)

        if self.ballPosition in self.goal1:
            score1 = 100
            score2 = -score1
            done = True
        elif self.ballPosition in self.goal2:
            score1 = -100
            score2 = -score1
            done = True
        else:
            score1 = score2 = 0
            done = False

        return self.state(), score1, score2, done


class FriendQLearner(QLearnerPrototype):
    def __init__(self, env=Soccer(), alphaDecay='exp'):
        super(FriendQLearner, self).__init__(env, alphaDecay)

    def train(self):
        done = False
        for episode in range(self.episodes):
            if done:
                self.env.new()

            position1 = self.env.player1.position
            position2 = self.env.player2.position
            possesion = int(self.env.possesion)
            Qp = self.Q1[2, 1, 1, 2, 4]

            action1 = np.random.choice(self.nA)
            action2 = np.random.choice(self.nA)

            newState, score1, score2, done = self.env.step(action1, action2)
            newPosition1, newPosition2, newPosession = newState

            self.Q1[position1, position2, possesion, action1, action2] = \
                (1 - self.alpha) * self.Q1[position1, position2, possesion, action1, action2] + \
                self.alpha * ((1 - self.gamma) * score1 + self.gamma * np.max(self.Q1[newPosition1, newPosition2, newPosession]))

            if [position1, position2, possesion, action1, action2] == [2, 1, 1, 2, 4]:
                self.difference.append(np.abs(self.Q1[2, 1, 1, 2, 4] - Qp))
                self.episode.append(episode)
                print("\rIteration %i alpha: %0.4f, difference: %0.4f " %
                      (episode, self.alpha, self.difference[-1]), end='')

            self.update_alpha(episode)
