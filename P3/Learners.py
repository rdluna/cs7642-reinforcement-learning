import matplotlib.pyplot as plt
from P3.QLearning import QLearner
from P3.FriendQLearning import FriendQLearner
from P3.FoeQLearning import FoeQLearner


def plotResults(learner, filename):
    plt.figure(figsize=(10, 10))
    plt.plot(learner.episode, learner.difference)
    plt.ylim([0, 0.5])
    plt.xlim([0, 1e06])
    plt.xlabel('Episode')
    plt.ylabel('Difference in state')
    plt.savefig('.\\Output\\' + filename)
    plt.close()


if __name__ == "__main__":

    for alphaDecay in ['exp', 'lin', 'log']:
        print(f"Running with {alphaDecay} alpha decay")

        stdQLearner = QLearner(alphaDecay=alphaDecay)
        stdQLearner.train()
        stdQLearner.save(f'QLearner_{alphaDecay}.csv')
        plotResults(stdQLearner, f'QLearner_{alphaDecay}.png')

        # friendQLearner = FriendQLearner(alphaDecay=alphaDecay)
        # friendQLearner.train()
        # friendQLearner.save(f'friendQLearner_{alphaDecay}.csv')
        # plotResults(friendQLearner, f'FriendQLearner_{alphaDecay}.png')
        #
        # foeQLearner = FoeQLearner(alphaDecay=alphaDecay)
        # foeQLearner.train()
        # foeQLearner.save(f'FoeQLearner_{alphaDecay}.csv')
        # plotResults(foeQLearner, f'FoeQLearner_{alphaDecay}.png')