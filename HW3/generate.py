import json

n = 30
states = range(n)

json_s = ''

for idx, state in enumerate(states):
    s_ = {'id': int(0), 'actions': []}
    t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
    a_ = {'id': int(0), 'transitions': []}
    if idx == 0:
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = int(1)
        t1['reward'] = int(0)
        t1['to'] = int(1)
        a1['transitions'].append(t1)
        a_ = {'id': int(0), 'transitions': []}
        a2 = a_.copy()
        a2['id'] = int(1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t2 = t_.copy()
        t2['id'] = int(0)
        t2['probability'] = int(1)
        t2['reward'] = int(0)
        t2['to'] = states[-1]
        a2['transitions'].append(t2)
        s['actions'].append(a1)
        s['actions'].append(a2)
        json_s += json.dumps(s)
    elif 0 < idx < int(len(states) / 2) - 1:
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = int(1)
        t1['reward'] = int(0)
        t1['to'] = int(idx + 1)
        a1['transitions'].append(t1)
        a_ = {'id': int(0), 'transitions': []}
        a2 = a_.copy()
        a2['id'] = int(1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t2 = t_.copy()
        t2['id'] = int(0)
        t2['probability'] = int(1)
        t2['reward'] = int(0)
        t2['to'] = states[-idx-1]
        a2['transitions'].append(t2)
        s['actions'].append(a1)
        s['actions'].append(a2)
        json_s += ',' + json.dumps(s)
    elif idx == int(len(states) / 2) - 1:
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = int(1)
        t1['reward'] = float(1)
        t1['to'] = int(idx + 1)
        a1['transitions'].append(t1)
        a_ = {'id': int(0), 'transitions': []}
        a2 = a_.copy()
        a2['id'] = int(1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t2 = t_.copy()
        t2['id'] = int(0)
        t2['probability'] = int(1)
        t2['reward'] = float(1)
        t2['to'] = states[-idx - 1]
        a2['transitions'].append(t2)
        s['actions'].append(a1)
        s['actions'].append(a2)
        json_s += ',' + json.dumps(s)
    elif idx == int(len(states) / 2):
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = int(1)
        t1['reward'] = int(0)
        t1['to'] = int(idx)
        a1['transitions'].append(t1)
        a_ = {'id': int(0), 'transitions': []}
        a2 = a_.copy()
        a2['id'] = int(1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t2 = t_.copy()
        t2['id'] = int(0)
        t2['probability'] = int(1)
        t2['reward'] = int(0)
        t2['to'] = states[idx]
        a2['transitions'].append(t2)
        s['actions'].append(a1)
        s['actions'].append(a2)
        json_s += ',' + json.dumps(s)
    elif idx == int(len(states) / 2) + 1:
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = int(1)
        t1['reward'] = int(0)
        t1['to'] = int(idx - 1)
        a1['transitions'].append(t1)
        a_ = {'id': int(0), 'transitions': []}
        a2 = a_.copy()
        a2['id'] = int(1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t2 = t_.copy()
        t2['id'] = int(0)
        t2['probability'] = int(1)
        t2['reward'] = int(0)
        t2['to'] = states[-idx + 1]
        a2['transitions'].append(t2)
        s['actions'].append(a1)
        s['actions'].append(a2)
        json_s += ',' + json.dumps(s)
    else:
        s = s_.copy()
        s['id'] = idx
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = float(0.5)
        t1['reward'] = int(0)
        t1['to'] = int(idx - 1)
        a1['transitions'].append(t1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(1)
        t1['probability'] = float(0.5)
        t1['reward'] = int(0)
        t1['to'] = int(-idx + len(states) + 1)
        a1['transitions'].append(t1)
        s['actions'].append(a1)
        a_ = {'id': int(0), 'transitions': []}
        a1 = a_.copy()
        a1['id'] = int(0)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(0)
        t1['probability'] = float(0.5)
        t1['reward'] = int(0)
        t1['to'] = int(idx - 1)
        a1['transitions'].append(t1)
        t_ = {'id': int(0), 'probability': int(1), 'reward': int(0), 'to': int(1)}
        t1 = t_.copy()
        t1['id'] = int(1)
        t1['probability'] = float(0.5)
        t1['reward'] = int(0)
        t1['to'] = int(-idx + len(states) + 1)
        a1['transitions'].append(t1)
        s['actions'].append(a1)
        json_s += ',' + json.dumps(s)

json_all = '{"gamma":0.75, "states":[' + json_s + ']}'

with open("output.json", 'w') as file:
    file.write(json.dumps(json.loads(json_all), indent=4))