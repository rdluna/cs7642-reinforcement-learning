import mdptoolbox
import numpy as np
import pandas as pd


def DieN(num_sides, bad_sides, num_rolls):
    num_actions = 2
    num_states = num_rolls * num_sides + 2
    good_sides = ~bad_sides + 2

    rewards = np.zeros((2, num_states, num_states))
    rewards_roll = np.zeros((rewards.shape[1], num_sides + 1))
    # The rewards from initial state (having $0) is the masking of the value for the dice
    # The first column will always have 0 reward
    rewards_roll[0, :] = np.hstack(([0], (np.arange(num_sides) + 1) * good_sides))
    # For each row with non-zero bankroll the rewards are shifted one state to the right for the first roll
    for i in range(num_sides + 1):
        rewards_roll[i + 1, :] = np.squeeze(pd.DataFrame(rewards_roll[i,:]).shift(1).fillna(0).values)
    # For subsequent rolls the last reward column for a single roll is shifted downwards
    for i in range(rewards.shape[2] - rewards_roll.shape[1]):
        rewards_roll = np.hstack((rewards_roll,pd.DataFrame(rewards_roll[:,-1]).shift(1).fillna(0).values))
    # The last column, corresponding to losing the game on a bad roll dice has the negative bankroll
    rewards_roll[:, -1] = np.arange(rewards_roll.shape[0]) * -1
    rewards[1] = rewards_roll

    # The probability is 1/n for each reward that doesn't reach a terminal state because they're fair dices
    p = 1.0 / num_sides

    transitions = np.zeros((num_actions, num_states, num_states))
    # The action of quitting is deterministic
    transitions[0] = np.eye(num_states, num_states)
    # Apply the dice probability to each reward that's not in the terminal state (the last column)
    transitions_roll = (rewards_roll != 0) * p
    # Calculate the probability of reaching the terminal state from the rest in each row
    transitions_roll[:, -1] = 1 - transitions_roll[:, :-1].sum(axis=1)
    transitions[1] = transitions_roll

    np.savetxt(r"D:\1.csv", transitions[1], delimiter=',')

    # Perform value iteration and calculate the maximum value after the number of runs
    vi = mdptoolbox.mdp.ValueIteration(transitions, rewards, 1)
    vi.run()
    print(vi.V[0])
    print(vi.policy)


DieN(6, np.array([1,1,1,0,0,0]), 1)
