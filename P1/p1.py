import numpy as np
import pandas as pd
import datetime


def td_lambda(alpha, lambda_, seq, v):
    e = np.zeros(7)
    delta = np.zeros(7)
    for step in range(0, len(seq) - 1):
        s0 = seq[step]
        s1 = seq[step + 1]
        e[s0] = e[s0] + 1.0
        td = alpha * (v[s1] - v[s0])
        delta = delta + (td * e)
        e = e*lambda_
    return delta


def random_walk():
    s = [3]
    while s[-1] != 0 and s[-1] != 6:
        if np.random.choice([True, False]):
            s.append(s[-1] + 1)
        else:
            s.append(s[-1] - 1)
    return s


s_prob = [1/6, 1/3, 1/2, 2/3, 5/6]
seeds = [69, 13]

for seed in seeds:

    np.random.seed(seed)
    now_string = str(datetime.datetime.today().strftime('%m%d%y%H%M%S'))

    training_sets = [[random_walk() for i in range(10)] for i in range(100)]

    alpha = 0.010
    lambdas = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    results = []
    for lambda_ in lambdas:
        errors = []
        for training_set in training_sets:
            values = np.zeros(7)
            innovation = 1
            while innovation > .001:
                values_0 = np.copy(values)
                update = np.zeros(7)
                values[-1] = 1.0
                for sequence in training_set:
                    update = update + td_lambda(alpha, lambda_, sequence, values)
                values = values + update
                innovation = np.sum(np.abs(values_0 - values))
            estimate = np.array(values[1:-1])
            error = np.sqrt(np.mean((s_prob - estimate) ** 2))
            errors.append(error)
        result = [lambda_, alpha, np.mean(errors)]
        results.append(result)

    data = pd.DataFrame(results, columns=["lambda", "alpha", "error"])
    data = data.sort_values(by='lambda')
    data.to_csv("exp1_" + now_string + ".csv", sep=',')

    iterations = [(alpha, lambda_) for alpha in [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6]
                  for lambda_ in [0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55,
                                  0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.00]]
    results = []

    for iteration in iterations:
        errors = []
        for training_set in training_sets:
            values = np.full(7, 0.5)
            for sequence in training_set:
                values[0] = 0
                values[-1] = 1
                values = values + td_lambda(iteration[0], iteration[1], sequence, values)
            estimate = np.array(values[1:-1])
            error = np.sqrt(np.mean((s_prob - estimate)**2))
            errors.append(error)
        result = [iteration[1], iteration[0], np.mean(errors)]
        results.append(result)

    data = pd.DataFrame(results)
    data.columns = ["lambda", "alpha", "error"]
    data.to_csv("exp2_" + now_string + ".csv", sep=',')

    data = data.groupby(data['lambda']).min()
    data.to_csv("exp3_" + now_string + ".csv", sep=',')
