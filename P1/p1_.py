import numpy as np
import matplotlib.pyplot as plt


def random_walk():
    s = [3]
    while s[-1] != 0 and s[-1] != 6:
        if np.random.choice([True, False]):
            s.append(s[-1] + 1)
        else:
            s.append(s[-1] - 1)
    return s


seeds = [69, 13]
steps1 = []
steps2 = []
len1 = []
len2 = []

for seed in seeds:
    np.random.seed(seed)
    training_sets = [[random_walk() for i in range(10)] for i in range(100)]
    for set_ in training_sets:
        for seq_ in set_:
            if seed == 69:
                len1.append(len(seq_))
            else:
                len2.append(len(seq_))
            for step in seq_:
                if seed == 69:
                    steps1.append(step)
                else:
                    steps2.append(step)


steps1 = np.array(steps1)
steps2 = np.array(steps2)
sums1 = []
sums2 = []
for i in np.arange(7):
    sums1.append(np.sum(steps1 == i))
for i in np.arange(7):
    sums2.append(np.sum(steps2 == i))

plt.rcParams.update({'font.size': 12})
plt.rcParams['axes.axisbelow'] = True

plt.figure(figsize=(12,3))
plt.subplot(121)
plt.grid(True)
plt.bar(np.arange(7), sums1)
plt.ylim([0, 3100])
plt.xticks(np.arange(7))
plt.yticks(np.arange(0, 3500, 500))
plt.title('Seed = 69')
plt.subplot(122)
plt.grid(True)
plt.bar(np.arange(7), sums2)
plt.ylim([0, 3100])
plt.xticks(np.arange(7))
plt.yticks(np.arange(0, 3500, 500))
plt.title('Seed = 13')
plt.savefig("walks1.png")
plt.figure(figsize=(12,3))
plt.subplot(121)
plt.hist(len1, bins=20, align='left')
plt.xlim([0,60])
plt.xticks(np.arange(0,70,10))
plt.yticks(np.arange(0, 450, 50))
plt.title('Seed = 69')
plt.grid(True)
plt.subplot(122)
plt.hist(len1, bins=20, align='left')
plt.xlim([0,60])
plt.xticks(np.arange(0,70,10))
plt.yticks(np.arange(0, 450, 50))
plt.title('Seed = 13')
plt.grid(True)
plt.savefig("walks2.png")