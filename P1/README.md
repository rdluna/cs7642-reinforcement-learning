The replication of the experiments by Sutton is implemented in two parts:

1) The main code is implemented in the python file p1.py , which has the code to run the TD(lambda) algorithm on random walks and generates the data used for the plots. You can run it standalone in any way (through an IDE or by python p1.py)
2) The plots are generated by the RMarkdown file report.Rmd, using the data generated by Python

There is an additional file called p1_.py which generates the summary plots for the random walk training sets

The Python scripts were run with Python 3.6.2 from the Anaconda distribution, the code requires the following libraries:
numpy 1.15.2
pandas 0.20.3
matplotlib 2.2.3

The R script to generate the report requires R-markdown as well as the following libraries:
ggplot2
latex2exp