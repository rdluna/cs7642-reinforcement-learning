import gym
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import FeatureUnion
from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler
import pandas as pd
import pickle
import time
import os


class QLearner:
    def __init__(self, env='LunarLander-v2', max_episodes=1000, gamma=0.99, eps=0.1, eps_decay=1):
        self.env = gym.make(env)
        self.max_episodes = max_episodes
        self.gamma = gamma
        self.eps = eps
        self.decay = eps_decay
        self.models = []
        self.episode_lengths = []
        self.episode_rewards = []
        self.episode_times = []
        self.max_steps = self.env.spec.tags.get('wrapper_config.TimeLimit.max_episode_steps')

    def sampleActions(self, samples=5000):
        if not os.path.isfile('samples.obj'):
            state_samples = []
            for _ in range(samples):
                print("\r It: " + str(_) + ", Len: " + str(len(state_samples)), end="")
                state = self.env.reset()
                state_samples.append(state)
                done = False
                while not done:
                    action = self.env.action_space.sample()
                    state, _, done, _ = self.env.step(action)
                    state_samples.append(state)
            state_samples = np.array(state_samples)
            with open('samples.obj', 'wb') as file:
                pickle.dump(state_samples, file)
        else:
            with open('samples.obj', 'rb') as file:
                state_samples = pickle.load(file)

        self.scaler = StandardScaler()
        self.scaler = self.scaler.fit(state_samples)
        self.feature_transform = FeatureUnion([
            ("rbf1", RBFSampler(gamma=0.50, n_components=100)),
            ("rbf2", RBFSampler(gamma=1.00, n_components=100)),
            ("rbf3", RBFSampler(gamma=2.00, n_components=100)),
            ("rbf4", RBFSampler(gamma=6.00, n_components=100))
        ])
        self.feature_transform = self.feature_transform.fit(self.scaler.transform(state_samples))

    def createModels(self):
        self.models = []
        for _ in range(self.env.action_space.n):
            model = SGDRegressor(learning_rate='constant')
            self.state = self.env.reset()
            model.partial_fit([self.transformState()], [0])
            self.models.append(model)

    def transformState(self):
        return self.feature_transform.transform(self.scaler.transform([self.state]))[0]

    def transformStateNext(self):
        return self.feature_transform.transform(self.scaler.transform([self.next_state]))[0]

    def modelPredict(self):
        transformedState = self.transformState()
        return np.array([model.predict([transformedState])[0] for model in self.models])

    def modelPredictNext(self):
        transformedState = self.transformStateNext()
        return np.array([model.predict([transformedState])[0] for model in self.models])

    def updateModel(self, target):
        transformedState = self.transformState()
        self.models[self.action].partial_fit([transformedState], [target])

    def makePolicy(self):
        def policy(self):
            policy = np.ones(self.env.action_space.n) * self.eps / self.env.action_space.n
            qVal = self.modelPredict()
            bestAction = np.argmax(qVal)
            policy[bestAction] += (1 - self.eps)
            return policy
        self.policy = policy

    def addEvidence(self):
        for episode in range(self.max_episodes):
            t1 = time.clock()
            self.eps = self.eps * self.decay**episode
            self.makePolicy()
            self.state = self.env.reset()
            self.episode_reward = 0
            for step in range(self.max_steps):
                self.action = np.random.choice(np.arange(len(self.policy(self))), p=self.policy(self))
                next_state, reward, done, _ = self.env.step(self.action)
                self.next_state = next_state
                self.episode_reward += reward
                update = self.modelPredictNext()
                self.target = reward + self.gamma * np.max(update)
                self.updateModel(self.target)
                print("\rEpisode: %s Step: %s Reward: %s" % (episode, step, self.episode_reward), end="")
                if done:
                    self.episode_times.append(time.clock() - t1)
                    self.episode_rewards.append(self.episode_reward)
                    self.episode_lengths.append(step)
                    break
                self.state = self.next_state

    def reset(self):
        self.models = []
        self.episode_lengths = []
        self.episode_rewards = []
        self.episode_times = []

    def save(self, fname):
        with open(fname, 'wb') as file:
            pickle.dump(self.models, file)


if __name__ == "__main__":
    learner = QLearner()
    learner.sampleActions()

    for i, gamma in enumerate([0.96, 0.97, 0.98, 0.99]):
        for j, eps in enumerate([0.40, 0.50, 0.60, 1.00]):
            for k, decay in enumerate([0.50, 0.75, 1.00]):
                fname = f'{i}{j}{k}'
                learner.gamma = gamma
                learner.eps = eps
                learner.decay = decay
                learner.createModels()
                learner.addEvidence()
                output = pd.DataFrame()
                output['reward'] = learner.episode_rewards
                output['length'] = learner.episode_lengths
                output['time'] = learner.episode_times
                output.to_csv(fname + '.csv', index=False)
                learner.save(fname + '.obj')
                learner.reset()
