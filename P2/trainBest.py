import pandas as pd
import pickle
from train import QLearner
import time
import numpy as np
from PIL import Image
import imageio
import matplotlib.pyplot as plt

class bestQLearner(QLearner):
    def __init__(self, env='LunarLander-v2', max_episodes=1000, gamma=0.99, eps=0.1, eps_decay=1):
        super(bestQLearner, self).__init__(env, max_episodes, gamma, eps, eps_decay)

    def addEvidence(self):
        for episode in range(self.max_episodes):
            t1 = time.clock()
            self.eps = self.eps * self.decay**episode
            self.makePolicy()
            self.state = self.env.reset()
            self.episode_reward = 0
            for step in range(self.max_steps):
                self.action = np.random.choice(np.arange(len(self.policy(self))), p=self.policy(self))
                next_state, reward, done, _ = self.env.step(self.action)
                self.next_state = next_state
                self.episode_reward += reward
                update = self.modelPredictNext()
                self.target = reward + self.gamma * np.max(update)
                self.updateModel(self.target)
                print("\rBest -> Episode: %s Step: %s Reward: %s" % (episode, step, self.episode_reward), end="")
                if done:
                    self.episode_times.append(time.clock() - t1)
                    self.episode_rewards.append(self.episode_reward)
                    self.episode_lengths.append(step)
                    break
                self.state = self.next_state
            if episode % 100 == 0:
                self.save('best.obj')

    def verify(self):
        print(f"Episodes : {self.max_episodes}")
        print(f"Gamma    : {self.gamma}")
        print(f"Eps      : {self.eps}")
        print(f"Decay    : {self.decay}")

    def plotEnv(self):
        plt.imshow(self.env.render('rgb_array'), cmap='gray', interpolation='none', clim=(0, 1))
        plt.axis('off')
        plt.gcf().set_facecolor('grey')
        canvas = plt.get_current_fig_manager().canvas
        canvas.draw()
        pil_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
        plt.close()
        return np.array(pil_image)

    def predict(self, episodes=100):
        self.predict_rewards = []
        self.predict_lengths = []
        self.predict_times = []
        for episode in range(episodes):
            t1 = time.clock()
            self.episode_images = []
            self.state = self.env.reset()
            self.episode_reward = 0
            self.episode_images.append(self.plotEnv())
            for step in range(self.max_steps):
                qVal = self.modelPredict()
                self.action = np.argmax(qVal)
                next_state, reward, done, _ = self.env.step(self.action)
                self.episode_images.append(self.plotEnv())
                self.episode_reward += reward
                if done:
                    self.predict_rewards.append(self.episode_reward)
                    self.predict_lengths.append(step)
                    self.predict_times.append(time.clock() - t1)
                    imageio.mimsave(f'episode_{episode}.gif', self.episode_images, duration=0.1)
                    break
                self.state = next_state


if __name__ == "__main__":
    learner = bestQLearner(max_episodes=4000, gamma=0.96, eps=0.40, eps_decay=1.00)
    learner.verify()
    learner.sampleActions()

    with open('002.obj', 'rb') as file:
        models = pickle.load(file)
    learner.models = models

    learner.addEvidence()
    output = pd.DataFrame()
    output['reward'] = learner.episode_rewards
    output['length'] = learner.episode_lengths
    output['time'] = learner.episode_times
    output.to_csv('best.csv', index=False)
    learner.save('best.obj')

    with open('best.obj', 'rb') as file:
        learner.models = pickle.load(file)

    learner.predict()
    output = pd.DataFrame()
    output['reward'] = learner.predict_rewards
    output['length'] = learner.predict_lengths
    output['time'] = learner.predict_times
    output.to_csv('pred.csv', index=False)