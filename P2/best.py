import pandas as pd
import pickle
import time
import numpy as np

print("HERE")

class bestQLearner(QLearner):
    def __init__(self):
        super(bestQLearner, self).__init__()

    def addEvidence(self):
        for episode in range(self.max_episodes):
            t1 = time.clock()
            self.eps = self.eps * self.decay**episode
            self.makePolicy()
            self.state = self.env.reset()
            self.episode_reward = 0
            for step in range(self.max_steps):
                self.action = np.random.choice(np.arange(len(self.policy(self))), p=self.policy(self))
                next_state, reward, done, _ = self.env.step(self.action)
                self.next_state = next_state
                self.episode_reward += reward
                update = self.modelPredictNext()
                self.target = reward + self.gamma * np.max(update)
                self.updateModel(self.target)
                print("\rBest -> Episode: %s Step: %s Reward: %s" % (episode, step, self.episode_reward), end="")
                if done:
                    self.episode_times.append(time.clock() - t1)
                    self.episode_rewards.append(self.episode_reward)
                    self.episode_lengths.append(step)
                    break
                self.state = self.next_state
            if episode % 100 == 0:
                self.save('best.obj')


# learner = bestQLearner(max_episodes=4000, gamma=0.96, eps=0.40, eps_decay=1.00)
# learner.sampleActions()

# with open('002.obj', 'rb') as file:
#     models = pickle.load(file)
# learner.models = models
#
# learner.addEvidence()
# output = pd.DataFrame()
# output['reward'] = learner.episode_rewards
# output['length'] = learner.episode_lengths
# output['time'] = learner.episode_times
# output.to_csv('best.csv', index=False)
# learner.save('best.obj')
