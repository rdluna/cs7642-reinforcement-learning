import mdptoolbox
import numpy as np
import scipy


def calcS0(p1, rewards, gamma=1):
    rewards1 = rewards.copy()
    rewards1[0] = 0
    rewards1[1] = rewards[0] * p1
    rewards1[2] = rewards[1] * (1 - p1)
    rewards1[3] = rewards[2] * p1 + rewards[3] * (1 - p1)
    rewards1[4:] = rewards[4:]

    num_states = len(rewards)

    transitions = np.genfromtxt(r"C:\Users\rodri\Documents\t.csv", delimiter=',')
    transitions = transitions.reshape((1, num_states, num_states))

    vi = mdptoolbox.mdp.ValueIteration(transitions, rewards1, gamma)
    vi.run()
    return rewards, vi.V[-1]


def calcR1(p1, rewards, values):
    r1 = rewards[0] + values[1] - values[0]
    r2 = rewards[1] + values[2] - values[0]
    return r1 * p1 + r2 * (1 - p1)


def calcR2(p1, rewards, values):
    r1 = rewards[0] + rewards[2] + values[3] - values[0]
    r2 = rewards[1] + rewards[3] + values[3] - values[0]
    return r1 * p1 + r2 * (1 - p1)


def calcR3(p1, rewards, values):
    r1 = rewards[0] + rewards[2] + rewards[4] + values[4] - values[0]
    r2 = rewards[1] + rewards[3] + rewards[4] + values[4] - values[0]
    return r1 * p1 + r2 * (1 - p1)


def calcR4(p1, rewards, values):
    r1 = rewards[0] + rewards[2] + rewards[4] + rewards[5] + values[5] - values[0]
    r2 = rewards[1] + rewards[3] + rewards[4] + rewards[5] + values[5] - values[0]
    return r1 * p1 + r2 * (1 - p1)


def calcR5(p1, rewards, values):
    r1 = rewards[0] + rewards[2] + rewards[4] + rewards[5] + rewards[6] + values[6] - values[0]
    r2 = rewards[1] + rewards[3] + rewards[4] + rewards[5] + rewards[6] + values[6] - values[0]
    return r1 * p1 + r2 * (1 - p1)


def calcRt(p1, rewards, values):
    r1 = rewards[0] + rewards[2] + rewards[4] + rewards[5] + rewards[6]
    r2 = rewards[1] + rewards[3] + rewards[4] + rewards[5] + rewards[6]
    return r1 * p1 + r2 * (1 - p1)


probToState = 0.41
valueEstimates = [0.0,20.1,0.0,13.0,24.6,19.7,15.0]
rewards = [-4.3,1.8,9.1,6.3,1.9,2.1,7.8]

rewards, s0 = calcS0(probToState, rewards)

R1 = calcR1(probToState, rewards, valueEstimates)
R2 = calcR2(probToState, rewards, valueEstimates)
R3 = calcR3(probToState, rewards, valueEstimates)
R4 = calcR4(probToState, rewards, valueEstimates)
R5 = calcR5(probToState, rewards, valueEstimates)
Rt = calcRt(probToState, rewards, valueEstimates)

print(s0)
print(R1)
print(R2)
print(R3)
print(R4)
print(R5)
print(Rt)

print([R1 - s0, R2 - R1, R3 - R2, R4 - R3, R5 - R4, Rt - R5])

p = scipy.poly1d(np.flip([R1 - s0, R2 - R1, R3 - R2, R4 - R3, R5 - R4, Rt - R5], axis=0))
print(scipy.roots(p))
